# OH WEEKLY ASSESSMENT 3

In this assessment, you have to answer some basic question about Javascript and solve some problems using Javascript. You can find the answer from the internet or by discussing with your friend, but make sure you understand what you are doing. Please deliver this assessment on time no matter what results you make! Thank you.

**Deadline**: Friday, 29 October 2021, 23:59 WIB

## Instructions

- Clone this project repository
- Try to complete complete the task
- Create git repository using `weekly-assessment-3` as the name of project
- Push your result to it
- Fill this form when you are finish: https://forms.gle/NLSnsuddjz1BPzeW6

## Task Requirements

- [x] Nilai rata-rata `score` dari `examData`
- [x] Nama siswa yang mendapatkan nilai `score` tertinggi
- [x] Nama siswa yang mendapatkan nilai `score` terendah
- [x] Nama siswa-siswa yang tidak lulus dengan nilai `score` minimal 75 untuk lulus
- [x] Jumlah persentase siswa yang lulus

## Task

Diberikan sebuah `examData` seperti di bawah ini:

```js
const examData = [
  { id: 1, name: 'Tony', score: 87 },
  { id: 2, name: 'Steve', score: 91 },
  { id: 3, name: 'Scott', score: 72 },
  { id: 4, name: 'Natasha', score: 66 },
  { id: 5, name: 'Bruce', score: 77 },
  { id: 6, name: 'Denvers', score: 82 },
  { id: 7, name: 'Pepper', score: 91 },
  { id: 8, name: 'Clint', score: 84 },
  { id: 9, name: 'Barton', score: 90 },
  { id: 10, name: 'Stacey', score: 88 },
  { id: 11, name: 'Wanda', score: 50 },
  { id: 12, name: 'Peter', score: 79 },
  { id: 13, name: 'James', score: 84 },
  { id: 14, name: 'Shang', score: 85 },
];
```

Buatlah perintah-perintah yang dapat menemukan hal-hal berikut:

1. Nilai rata-rata `score` dari `examData`
2. Nama siswa yang mendapatkan nilai `score` tertinggi
3. Nama siswa yang mendapatkan nilai `score` terendah
4. Nama siswa-siswa yang tidak lulus dengan nilai `score` minimal 75 untuk lulus
5. Jumlah persentase siswa yang lulus

> Kerjakan dalam `examData.js`
